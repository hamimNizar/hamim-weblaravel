<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/porto', function () {

    $portofolios = [
        ['titleporto' => 'Blood4Life',
        'containporto' => 'Website project using template for donating blood',
        'timeporto' => '3 January 2021'],
        ['titleporto' => 'ImmuneApp',
        'containporto' => 'Android Mobile App project for self test about covid-19',
        'timeporto' => '12 February 2021'],
        ['titleporto' => 'InfoKos',
        'containporto' => 'Website project using template for admin Kost',
        'timeporto' => '3 March 2021'],
        ['titleporto' => 'Slot Games',
        'containporto' => 'Android mobile App project for dice roll',
        'timeporto' => '5 March 2021'],
        ['titleporto' => 'TokoAdmin',
        'containporto' => 'Android mobile App project for admin store',
        'timeporto' => '21 April 2021'],
        ['titleporto' => 'InfoKos',
        'containporto' => 'Website project using template for admin Kost',
        'timeporto' => '3 May 2021']
    ];

    return view('porto', ['portofolios' => $portofolios]);
});

Route::get('/contact', function () {
    return view('contact');
});
