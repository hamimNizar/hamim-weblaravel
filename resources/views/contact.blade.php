@extends('layout/main')

@section('title', 'Contact Page')

@section('container')
<div class="container">
  <div class="card mt-5">
    <div class="row g-0">
      <div class="col-md-3">
        <img src="{{ asset('assets/images/gbr-ku.jpg') }}" alt="hny" style="width: 240px;">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h1 class="card-title">Find Me on :</h1>
                    
          <!-- <dl class="row mt-5">
            <dt class="col-sm-3"><h3><i class="bi bi-envelope"></i></h3></dt>
            <dd class="col-sm-9"><h3>hamim.hny@gmail.com</h3></dd>

            <dt class="col-sm-3">Faculty</dt>
            <dd class="col-sm-9">Faculty of Computer Science</dd>

            <dt class="col-sm-3">Study Program</dt>
            <dd class="col-sm-9">Information Technology</dd>

          </dl> -->
          <ul class="list-group mt-5">
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <h3><i class="bi bi-envelope"></i></h3>
              <span class="badge bg-primary rounded-pill">hamim.hny@gmail.com</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <h3><i class="bi bi-instagram"></i></h3>
              <span class="badge bg-primary rounded-pill">@hamimnizary</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <h3><i class="bi bi-linkedin"></i></h3>
              <span class="badge bg-primary rounded-pill">linkedin.com/hamimnizar</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <h3><i class="bi bi-github"></i></h3>
              <span class="badge bg-primary rounded-pill">@hamimNizar</span>
            </li>
          </ul>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
