@extends('layout/main')

@section('title', 'Protofolio Page')

@section('container')
<div class="container">
  <div class="row">
    @for($a = 0; $a < count($portofolios); $a++)
    <div class="col-md-3 mt-5">
      <div class="card text-center">
        <div class="card-header">
        {{ $a + 1 }}
        </div>
        <div class="card-body">
          <h5 class="card-title">{{ $portofolios[$a]['titleporto'] }}</h5>
          <p class="card-text">{{ $portofolios[$a]['containporto'] }}</p>
          <a href="#" class="btn btn-primary">Detail</a>
        </div>
        <div class="card-footer text-muted">
        {{ $portofolios[$a]['timeporto'] }}
        </div>
      </div>
    </div>
    @endfor
  </div>
</div>
@endsection
