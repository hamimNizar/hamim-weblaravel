@extends('layout/main')

@section('title', 'Welcome Page')

@section('container')
<div class="container">
  <div class="card mt-5">
    <div class="row g-0">
      <div class="col-md-3">
        <img src="{{ asset('assets/images/gbr-ku.jpg') }}" alt="hny" style="width: 240px;">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h1 class="card-title">Hamim Nizar Yudistira</h1>
          <p class="card-text">This is a home page of my project laravel website. 
          This website was built to fulfill advanced web programming course assignments.
          on this website you will find a home page, a portfolio page to view my portfolio, and a contact page to view my social media.</p>
          
          <dl class="row">
            <dt class="col-sm-3">College</dt>
            <dd class="col-sm-9">Brawijaya university</dd>

            <dt class="col-sm-3">Faculty</dt>
            <dd class="col-sm-9">Faculty of Computer Science</dd>

            <dt class="col-sm-3">Study Program</dt>
            <dd class="col-sm-9">Information Technology</dd>

          </dl>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
